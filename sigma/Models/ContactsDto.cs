﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sigma.Models
{
    public class ContactsDto
    {
        [Key]
        public int id { get; set; }
        [Display(Name = "Nombre*:")]
        [Required(ErrorMessage = "Debe ingresar el nombre:")]
        [StringLength(50)]
        public string name { get; set; }

        [Display(Name = "Departamento*:")]
        [Required(ErrorMessage = "Debe seleccionar departamento:")]
        [StringLength(30)]
        public string state { get; set; }
        [Display(Name = "Ciudad*:")]
        [Required(ErrorMessage = "Debe ingresar la ciudad:")]
        [StringLength(50)]
        public string city { get; set; }
        [Display(Name = "Email*:")]
        [Required(ErrorMessage = "Debe ingresar el email:")]
        [StringLength(30)]
        [EmailAddress]
        public string email { get; set; }
        public List<SelectListItem> departamentolista { get; set; }
        public List<SelectListItem> ciudadlista { get; set; }
    }
}
