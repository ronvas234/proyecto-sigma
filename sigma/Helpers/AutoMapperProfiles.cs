﻿using AutoMapper;
using sigma.Entity;
using sigma.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sigma.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Contacts, ContactsDto>().ReverseMap();
        }
    }
}
