﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Serialization.Json;
using sigma.Data;
using sigma.Entity;
using sigma.Models;

namespace sigmacode.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        public HomeController(ILogger<HomeController> logger,ApplicationDbContext context, IMapper mapper)
        {
            _logger = logger;
            _context = context;
            _mapper = mapper;
        }

        public ActionResult Index()
        {
            var model = new ContactsDto
            {
                ciudadlista=new List<SelectListItem>(),
                departamentolista=new List<SelectListItem>()
            };
            return View(model);
        }
        public ActionResult GetDepartamentoCiudad()
        {
            var client = new RestClient("https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);
            var n = JObject.Parse(response.Content);
            return Json(n);
        }
        [HttpPost]
        public async Task<ActionResult> GuardarDatos(ContactsDto model)
        {
            JsonResult result;
            var men = "";
            try
            {
                if (ModelState.IsValid)
                {
                    var query = _context.contacts.Where(x => x.email == model.email.Trim()).FirstOrDefault();
                    if (query != null) {
                        men = "El correo ya esta registrado";
                    }
                    else
                    {
                        var dto = _mapper.Map<Contacts>(model);
                        await _context.AddAsync(dto);
                        _context.SaveChanges();
                    }

                }
                else
                {
                    men = String.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                                                           .Select(v => v.ErrorMessage + " " + v.Exception));
                }
            }
            catch (Exception ex)
            {
                    men = ex.Message;
            }
            result = new JsonResult(men);
            return result;
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
