﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sigma.Entity
{
    public class Contacts
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string email { get; set; }
    }
}
