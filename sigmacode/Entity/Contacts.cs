﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sigmacode.Entity
{
    public class Contacts
    {
        [Key]
        public int id { get; set; }
        public string nombre { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string email { get; set; }
    }
}
