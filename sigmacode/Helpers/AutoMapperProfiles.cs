﻿using AutoMapper;
using sigmacode.Entity;
using sigmacode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sigmacode.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Contacts, ContactsDto>().ReverseMap();
        }
    }
}
