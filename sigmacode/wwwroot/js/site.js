﻿function abrirmodal(title, cuerpo, footer) {
    $("#mtitle").empty();
    $("#mtitle").html(title);
    $("#midcuerpo").empty();
    $("#midcuerpo").html(cuerpo);
    $("#mfooter").empty();
    $("#mfooter").html(footer);
    if (!$('#formensmodal').is(":visible")) {
        $("#formensmodal").modal("show");
    }
}
function cerrarmodal() {
    $("#formensmodal").modal("hide");
};